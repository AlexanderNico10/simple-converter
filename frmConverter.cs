﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Converter
{
    public partial class frmConverter : Form
    {
        bool showCurMsg = false;
        
        public frmConverter()
        {
            InitializeComponent();
            txtAmount1.Click += new System.EventHandler(txtAmount1_Click);


            List<string> names = new List<string>();
            names.Add("Currency");
			names.Add("Area");
			names.Add("Energy");
            names.Add("Length");
            names.Add("Mass");
            names.Add("Power");
			names.Add("Temperature");

            ddlUnitType.DataSource = names;
			ddlUnitType.SelectedIndex = 3;

            if (ddlUnitType.Text == "Length")
            {
                List<string> lengthList = new List<string>();
                lengthList.Add("Metres");
                lengthList.Add("Kilometres");
                lengthList.Add("Inches");
                lengthList.Add("Feet");
                lengthList.Add("Yards");
                lengthList.Add("Miles");

                ddlUnit1.DataSource = lengthList;
                ddlUnit2.DataSource = new List<string>(lengthList);

                ddlUnit1.SelectedIndex = 1;
                ddlUnit2.SelectedIndex = 5;
            }

            ddlUnitType.SelectedIndexChanged += new System.EventHandler(ddlUnitType_SelectedIndexChanged);
            ddlUnit1.SelectedIndexChanged += new System.EventHandler(ddlUnit_SelectedIndexCHanged);
            ddlUnit2.SelectedIndexChanged += new System.EventHandler(ddlUnit_SelectedIndexCHanged);
        }

        private void ddlUnit_SelectedIndexCHanged(object sender, System.EventArgs e)
        {
            txtAmount1.Text = "0";
            lblAmountAns2.Text = "0";
        }

        private void txtAmount1_Click(object sender, System.EventArgs e)
        {
            txtAmount1.SelectAll();
        }

        private void ddlUnitType_SelectedIndexChanged(object sender, System.EventArgs e) {
            txtAmount1.Text = "0";
            lblAmountAns2.Text = "0";

			if (ddlUnitType.Text == "Currency")
            {
                List<string> currencyList = new List<string>();
                currencyList.Add("USD - US Dollar");    // US Dollar
                currencyList.Add("ZAR - South African Rand");    // South African Rand
                currencyList.Add("EUR - Eurozone Euro");    // Eurozone Euro
                currencyList.Add("GBP - Great British Pound");    // Great Britain Pound
                currencyList.Add("JPY - Japanese Yen");    // Japan Yen
                currencyList.Add("CNY - Chinese Yuan");    // China Yuan
                
                ddlUnit1.DataSource = currencyList;
                ddlUnit2.DataSource = new List<string>(currencyList);

                ddlUnit2.SelectedIndex = 1;

                if (showCurMsg == false)
                {
                    MessageBox.Show("Foreign currency conversion requires an internet connection.", "Internet connection required");
                    showCurMsg = true;
                }
            }
			
			if (ddlUnitType.Text == "Area")
            {
                List<string> areaList = new List<string>();
                areaList.Add("Square Feet");
                areaList.Add("Square Miles"); 
                areaList.Add("Square Metres");
                areaList.Add("Square Kilometres");
                areaList.Add("Acres");
                
                ddlUnit1.DataSource = areaList;
                ddlUnit2.DataSource = new List<string>(areaList);

				ddlUnit1.SelectedIndex = 3;
                ddlUnit2.SelectedIndex = 1;
            }

			if (ddlUnitType.Text == "Energy")
            {
                List<string> energyList = new List<string>();
                energyList.Add("Kilocalories");
                energyList.Add("Kilojoules"); 
                
                ddlUnit1.DataSource = energyList;
                ddlUnit2.DataSource = new List<string>(energyList);

                ddlUnit2.SelectedIndex = 1;
            }

            if (ddlUnitType.Text == "Length")
            {
                List<string> lengthList = new List<string>();
                lengthList.Add("Metres");
                lengthList.Add("Kilometres");
                lengthList.Add("Inches");
                lengthList.Add("Feet");
                lengthList.Add("Yards");
                lengthList.Add("Miles");

                ddlUnit1.DataSource = lengthList;
                ddlUnit2.DataSource = new List<string>(lengthList);

                ddlUnit1.SelectedIndex = 1;
                ddlUnit2.SelectedIndex = 5;
            }

            if (ddlUnitType.Text == "Mass")
            {
                List<string> massList = new List<string>();
                massList.Add("Kilogrammes");
                massList.Add("Pounds");
                massList.Add("Tonnes");

                ddlUnit1.DataSource = massList;
                ddlUnit2.DataSource = new List<string>(massList);

                ddlUnit2.SelectedIndex = 1;
            }

            if (ddlUnitType.Text == "Power")
            {
                List<string> powerList = new List<string>();
                powerList.Add("Kilowatts");
                powerList.Add("Horse Power");

                ddlUnit1.DataSource = powerList;
                ddlUnit2.DataSource = new List<string>(powerList);

                ddlUnit2.SelectedIndex = 1;
            }
			
			if (ddlUnitType.Text == "Temperature")
            {
                List<string> temperatureList = new List<string>();
                temperatureList.Add("Celsius");
                temperatureList.Add("Fahrenheit");
				temperatureList.Add("Kelvin");

                ddlUnit1.DataSource = temperatureList;
                ddlUnit2.DataSource = new List<string>(temperatureList);

                ddlUnit2.SelectedIndex = 1;
            }
        }

        private void currency(double originalValue)
        {
            string response;
            double exchangeRate;

            try
            {
                switch (ddlUnit1.Text) // First unit of measurement
                {
                    // Second unit of measurement
                    case "USD - US Dollar": switch (ddlUnit2.Text)
                    {
                        case "USD - US Dollar": lblAmountAns2.Text = "$ " + originalValue.ToString();
                            break;
                        case "ZAR - South African Rand": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=USDZAR=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "R " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                        case "EUR - Eurozone Euro": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=USDEUR=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "€ " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                        case "GBP - Great British Pound": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=USDGBP=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "£ " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                        case "JPY - Japanese Yen": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=USDJPY=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "¥ " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                        case "CNY - Chinese Yuan": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=USDCNY=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "¥ " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                    }
                    break;

                    case "ZAR - South African Rand": switch (ddlUnit2.Text)
                    {
                        case "USD - US Dollar": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=ZARUSD=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "$ " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                        case "ZAR - South African Rand": lblAmountAns2.Text = "R " + originalValue.ToString();
                            break;
                        case "EUR - Eurozone Euro": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=ZAREUR=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "€ " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                        case "GBP - Great British Pound": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=ZARGBP=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "£ " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                        case "JPY - Japanese Yen": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=ZARJPY=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "¥ " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                        case "CNY - Chinese Yuan": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=ZARCNY=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "¥ " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                    }
                    break;

                    case "EUR - Eurozone Euro": switch (ddlUnit2.Text)
                    {
                        case "USD - US Dollar": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=EURUSD=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "$ " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                        case "ZAR - South African Rand": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=EURZAR=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "R " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                        case "EUR - Eurozone Euro": lblAmountAns2.Text = "€ " + originalValue.ToString();
                            break;
                        case "GBP - Great British Pound": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=EURGBP=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "£ " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                        case "JPY - Japanese Yen": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=EURJPY=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "¥ " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                        case "CNY - Chinese Yuan": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=EURCNY=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "¥ " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                    }
                    break;

                    case "GBP - Great British Pound": switch (ddlUnit2.Text)
                    {
                        case "USD - US Dollar": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=GBPUSD=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "$ " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                        case "ZAR - South African Rand": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=GBPZAR=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "R " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                        case "EUR - Eurozone Euro": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=GBPEUR=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "€ " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                        case "GBP - Great British Pound": lblAmountAns2.Text = "£ " + originalValue.ToString();
                            break;
                        case "JPY - Japanese Yen": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=GBPJPY=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "¥ " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                        case "CNY - Chinese Yuan": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=GBPCNY=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "¥ " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                    }
                    break;

                    case "JPY - Japanese Yen": switch (ddlUnit2.Text)
                    {
                        case "USD - US Dollar": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=JPYUSD=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "$ " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                        case "ZAR - South African Rand": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=JPYZAR=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "R " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                        case "EUR - Eurozone Euro": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=JPYEUR=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "€ " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                        case "GBP - Great British Pound": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=JPYGBP=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "£ " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                        case "JPY - Japanese Yen": lblAmountAns2.Text = "¥ " + originalValue.ToString();
                            break;
                        case "CNY - Chinese Yuan": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=JPYCNY=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "¥ " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                    }
                    break;

                    case "CNY - Chinese Yuan": switch (ddlUnit2.Text)
                    {
                        case "USD - US Dollar": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=CNYUSD=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "$ " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                        case "ZAR - South African Rand": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=CNYZAR=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "R " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                        case "EUR - Eurozone Euro": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=CNYEUR=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "€ " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                        case "GBP - Great British Pound": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=CNYGBP=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "£ " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                        case "JPY - Japanese Yen": response = new System.Net.WebClient().DownloadString("http://finance.yahoo.com/d/quotes.csv?s=CNYJPY=X&f=l1");
                            exchangeRate = double.Parse(response, System.Globalization.CultureInfo.InvariantCulture);
                            lblAmountAns2.Text = "¥ " + Math.Round((originalValue * exchangeRate), 2).ToString();
                            break;
                        case "CNY - Chinese Yuan": lblAmountAns2.Text = "¥ " + originalValue.ToString();
                            break;
                    }
                    break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("The conversion rate could not be obtained. Check your internet connection.", "Check your internet connection");
            }
        }

        private void area(double originalValue)
        {
            switch (ddlUnit1.Text)
            {
                case "Square Feet": switch (ddlUnit2.Text)
                {
                    case "Square Feet": lblAmountAns2.Text = originalValue.ToString() + " sq. ft.";
                        break;
                    case "Square Miles": lblAmountAns2.Text = (originalValue * 0.03581 * 0.000001).ToString() + " sq. MI.";
                        break;
                    case "Square Metres": lblAmountAns2.Text = (originalValue * 0.0929).ToString() + " sq. M.";
                        break;
                    case "Square Kilometres": lblAmountAns2.Text = (originalValue * 0.0929 * 0.000001).ToString() + " sq. KM.";
                        break;
                    case "Acres": lblAmountAns2.Text = (originalValue * 0.02296 * 0.001).ToString() + " acres";
                        break;
                }
                break;

                case "Square Miles": switch (ddlUnit2.Text)
                {
                    case "Square Feet": lblAmountAns2.Text = (originalValue * 27878400).ToString() + " sq. ft.";
                        break;
                    case "Square Miles": lblAmountAns2.Text = originalValue.ToString() + " sq. MI.";
                        break;
                    case "Square Metres": lblAmountAns2.Text = (originalValue * 2589988.1103).ToString() + " sq. M.";
                        break;
                    case "Square Kilometres": lblAmountAns2.Text = (originalValue * 2.58999).ToString() + " sq. KM.";
                        break;
                    case "Acres": lblAmountAns2.Text = (originalValue * 640).ToString() + " acres";
                        break;
                }
                break;

                case "Square Metres": switch (ddlUnit2.Text)
                {
                    case "Square Feet": lblAmountAns2.Text = (originalValue * 10.76391).ToString() + " sq. ft.";
                        break;
                    case "Square Miles": lblAmountAns2.Text = (originalValue * 0.3861 * 0.000001).ToString() + " sq. MI.";
                        break;
                    case "Square Metres": lblAmountAns2.Text = originalValue.ToString() + " sq. M.";
                        break;
                    case "Square Kilometres": lblAmountAns2.Text = (originalValue * 0.000001).ToString() + " sq. KM.";
                        break;
                    case "Acres": lblAmountAns2.Text = (originalValue * 0.24711 * 0.001).ToString() + " acres";
                        break;
                }
                break;

                case "Square Kilometres": switch (ddlUnit2.Text)
                {
                    case "Square Feet": lblAmountAns2.Text = (originalValue * 10763910.417).ToString() + " sq. ft.";
                        break;
                    case "Square Miles": lblAmountAns2.Text = (originalValue * 0.3861).ToString() + " sq. MI.";
                        break;
                    case "Square Metres": lblAmountAns2.Text = (originalValue * 1000000).ToString() + " sq. M.";
                        break;
                    case "Square Kilometres": lblAmountAns2.Text = originalValue.ToString() + " sq. KM.";
                        break;
                    case "Acres": lblAmountAns2.Text = (originalValue * 247.10538).ToString() + " acres";
                        break;
                }
                break;

                case "Acres": switch (ddlUnit2.Text)
                {
                    case "Square Feet": lblAmountAns2.Text = (originalValue * 43560).ToString() + " sq. ft.";
                        break;
                    case "Square Miles": lblAmountAns2.Text = (originalValue * 0.00156).ToString() + " sq. MI.";
                        break;
                    case "Square Metres": lblAmountAns2.Text = (originalValue * 4046.85642).ToString() + " sq. M.";
                        break;
                    case "Square Kilometres": lblAmountAns2.Text = (originalValue * 0.004047).ToString() + " sq. KM.";
                        break;
                    case "Acres": lblAmountAns2.Text = originalValue.ToString() + " acres";
                        break;
                }
                break;
            }
        }

        private void energy(double originalValue)
        {
            switch (ddlUnit1.Text)
            {
                case "Kilocalories": switch (ddlUnit2.Text)
                {
                    case "Kilocalories": lblAmountAns2.Text = originalValue.ToString() + " kcal.";
                        break;
                    case "Kilojoules": lblAmountAns2.Text = (originalValue * 4.1868).ToString() + " kj";
                        break;
                }
                break;

                case "Kilojoules": switch (ddlUnit2.Text)
                {
                    case "Kilocalories": lblAmountAns2.Text = (originalValue * 0.23885).ToString() + " kcal.";
                        break;
                    case "Kilojoules": lblAmountAns2.Text = originalValue.ToString() + " kj";
                        break;
                }
                break;
            }
        }

        private void length(double originalValue) {
            
            switch (ddlUnit1.Text) // First unit of measurement
            {
                // Second unit of measurement
                case "Metres": switch (ddlUnit2.Text)
                {
                    case "Metres": lblAmountAns2.Text = originalValue.ToString() + " M";
                        break;
                    case "Kilometres": lblAmountAns2.Text = (originalValue * 0.001).ToString() + " KM";
                        break;
                    case "Inches": lblAmountAns2.Text = (originalValue * 39.37008).ToString() + " in";
                        break;
                    case "Feet": lblAmountAns2.Text = (originalValue * 3.28084).ToString() + " ft";
                        break;
                    case "Yards": lblAmountAns2.Text = (originalValue * 1.09361).ToString() + " yds";
                        break;
                    case "Miles": lblAmountAns2.Text = (originalValue * 0.62137 * 0.001).ToString() + " M";
                        break;
                }
                break;

                case "Kilometres": switch (ddlUnit2.Text)
                {
                    case "Metres": lblAmountAns2.Text = (originalValue * 1000).ToString() + " M";
                        break;
                    case "Kilometres": lblAmountAns2.Text = originalValue.ToString() + " KM";
                        break;
                    case "Inches": lblAmountAns2.Text = (originalValue * 39.37008 * 1000).ToString() + " in";
                        break;
                    case "Feet": lblAmountAns2.Text = (originalValue * 3.28084 * 1000).ToString() + " ft";
                        break;
                    case "Yards": lblAmountAns2.Text = (originalValue * 1.09361 * 1000).ToString() + " yds";
                        break;
                    case "Miles": lblAmountAns2.Text = (originalValue * 0.62137).ToString() + " M";
                        break;
                }
                break;

                case "Inches": switch (ddlUnit2.Text)
                {
                    case "Metres": lblAmountAns2.Text = (originalValue * 0.0254).ToString() + " M";
                        break;
                    case "Kilometres": lblAmountAns2.Text = (originalValue * 0.0254 * 0.001).ToString() + " KM";
                        break;
                    case "Inches": lblAmountAns2.Text = originalValue.ToString() + " in";
                        break;
                    case "Feet": lblAmountAns2.Text = (originalValue * 0.08333).ToString() + " ft";
                        break;
                    case "Yards": lblAmountAns2.Text = (originalValue * 0.02778).ToString() + " yds";
                        break;
                    case "Miles": lblAmountAns2.Text = (originalValue * 0.01578 * 0.001).ToString() + " M";
                        break;
                }
                break;

                case "Feet": switch (ddlUnit2.Text)
                {
                    case "Metres": lblAmountAns2.Text = (originalValue * 0.3048).ToString() + " M";
                        break;
                    case "Kilometres": lblAmountAns2.Text = (originalValue * 0.3048 * 0.001).ToString() + " KM";
                        break;
                    case "Inches": lblAmountAns2.Text = (originalValue * 12).ToString() + " in";
                        break;
                    case "Feet": lblAmountAns2.Text = originalValue.ToString() + " ft";
                        break;
                    case "Yards": lblAmountAns2.Text = (originalValue * 0.33333).ToString() + " yds";
                        break;
                    case "Miles": lblAmountAns2.Text = (originalValue * 0.18939 * 0.001).ToString() + " M";
                        break;
                }
                break;

                case "Yards": switch (ddlUnit2.Text)
                {
                    case "Metres": lblAmountAns2.Text = (originalValue * 0.9144).ToString() + " M";
                        break;
                    case "Kilometres": lblAmountAns2.Text = (originalValue * 0.9144 * 0.001).ToString() + " KM";
                        break;
                    case "Inches": lblAmountAns2.Text = (originalValue * 36).ToString() + " in";
                        break;
                    case "Feet": lblAmountAns2.Text = (originalValue * 3).ToString() + " ft";
                        break;
                    case "Yards": lblAmountAns2.Text = originalValue.ToString() + " yds";
                        break;
                    case "Miles": lblAmountAns2.Text = (originalValue * 0.56818 * 0.001).ToString() + " M";
                        break;
                }
                break;

                case "Miles": switch (ddlUnit2.Text)
                {
                    case "Metres": lblAmountAns2.Text = (originalValue * 1609.344).ToString() + " M";
                        break;
                    case "Kilometres": lblAmountAns2.Text = (originalValue * 1609.344 *0.001).ToString() + " KM";
                        break;
                    case "Inches": lblAmountAns2.Text = (originalValue * 63360).ToString() + " in";
                        break;
                    case "Feet": lblAmountAns2.Text = (originalValue * 5280).ToString() + " ft";
                        break;
                    case "Yards": lblAmountAns2.Text = (originalValue * 1760).ToString() + " yds";
                        break;
                    case "Miles": lblAmountAns2.Text = originalValue.ToString() + " M";
                        break;
                }
                break;
            }
        }
        

        private void mass(double originalValue)
        {
            switch (ddlUnit1.Text)
            {
                case "Kilogrammes": switch (ddlUnit2.Text)
                {
                    case "Kilogrammes": lblAmountAns2.Text = originalValue.ToString() + " kg";
                        break;
                    case "Pounds": lblAmountAns2.Text = (originalValue * 2.20462).ToString() + " lbs.";
                        break;
                    case "Tonnes": lblAmountAns2.Text = (originalValue * 0.001).ToString() + " tonnes";
                        break;
                }
                break;

                case "Pounds": switch (ddlUnit2.Text)
                {
                    case "Kilogrammes": lblAmountAns2.Text = (originalValue * 0.45359).ToString() + "kg";
                        break;
                    case "Pounds": lblAmountAns2.Text = originalValue.ToString() + " lbs.";
                        break;
                    case "Tonnes": lblAmountAns2.Text = (originalValue * 0.45359 * 0.001).ToString() + " tonnes";
                        break;
                }
                break;

                case "Tonnes": switch (ddlUnit2.Text)
                {
                    case "Kilogrammes": lblAmountAns2.Text = (originalValue * 1000).ToString() + "kg";
                        break;
                    case "Pounds": lblAmountAns2.Text = (originalValue * 2204.62262).ToString() + " lbs.";
                        break;
                    case "Tonnes": lblAmountAns2.Text = originalValue.ToString() + " tonnes";
                        break;
                }
                break;
            }
        }

        private void power(double originalValue)
        {
            switch (ddlUnit1.Text) // First unit of measurement
            {
                // Second unit of measurement
                case "Kilowatts": switch (ddlUnit2.Text) 
                {
                    case "Kilowatts": lblAmountAns2.Text = originalValue.ToString() + " KW";
                        break;
                    case "Horse Power": lblAmountAns2.Text = (originalValue * 1.34102).ToString() + " HP";
                        break;
                }
                break;

                case "Horse Power": switch (ddlUnit2.Text)
                {
                    case "Horse Power": lblAmountAns2.Text = originalValue.ToString() + " HP";
                        break;
                    case "Kilowatts": lblAmountAns2.Text = (originalValue * 0.7457).ToString() + " KW";
                        break;
                }
                break;
            }
        }

        private void temperature(double originalValue)
        {
            switch (ddlUnit1.Text)
            {
                case "Celsius": switch (ddlUnit2.Text)
                    {
                        case "Celsius": lblAmountAns2.Text = originalValue.ToString() + " °C";
                            break;
                        case "Fahrenheit": lblAmountAns2.Text = ((originalValue * 1.8)+32).ToString() + " °F";
                            break;
                        case "Kelvin": lblAmountAns2.Text = (originalValue + 273.15).ToString() + " K";
                            break;
                    }
                    break;

                case "Fahrenheit": switch (ddlUnit2.Text)
                    {
                        case "Celsius": lblAmountAns2.Text = ((originalValue - 32) / 1.8).ToString() + " °C";
                            break;
                        case "Fahrenheit": lblAmountAns2.Text = originalValue.ToString() + " °F";
                            break;
                        case "Kelvin": lblAmountAns2.Text = (((originalValue-32)/1.8) + 273.15).ToString() + " K";
                            break;
                    }
                    break;

                case "Kelvin": switch (ddlUnit2.Text)
                    {
                        case "Celsius":
                            if (originalValue >= 0)
                            {
                                lblAmountAns2.Text = (originalValue - 273.15).ToString() + " °C";
                            }
                            else
                            {
                                MessageBox.Show("Cannot enter a negative Kelvin value.","Invalid Kelvin Value");
                                txtAmount1.Text = "0";
                                lblAmountAns2.Text = "0";
                            }
                            break;
                        case "Fahrenheit":
                            if (originalValue >= 0)
                            {
                                lblAmountAns2.Text = (((originalValue - 273.15)* 1.8) + 32).ToString() + " °F";
                            }
                            else
                            {
                                MessageBox.Show("Cannot enter a negative Kelvin value.", "Invalid Kelvin Value");
                                txtAmount1.Text = "0";
                                lblAmountAns2.Text = "0";
                            }
                            break;
                        case "Kelvin":
                            if (originalValue >= 0)
                            {
                                lblAmountAns2.Text = originalValue.ToString() + " K";
                            }
                            else
                            {
                                MessageBox.Show("Cannot enter a negative Kelvin value.", "Invalid Kelvin Value");
                                txtAmount1.Text = "0";
                                lblAmountAns2.Text = "0";
                            }
                            break;
                    }
                    break;
            }
        }

        private void btnConvert_Click(object sender, EventArgs e)
        {
            double originalValue;
            bool isNumeric = double.TryParse(txtAmount1.Text, out originalValue);

            if (isNumeric)
            {
                originalValue = double.Parse(txtAmount1.Text);

                if (ddlUnitType.Text == "Currency")
                {
                    currency(originalValue);
                }
                else if (ddlUnitType.Text == "Area")
                {
                    area(originalValue);
                }
                else if (ddlUnitType.Text == "Energy")
                {
                    energy(originalValue);
                }
                else if (ddlUnitType.Text == "Length")
                {
                    length(originalValue);
                }
                else if (ddlUnitType.Text == "Mass")
                {
                    mass(originalValue);
                }
                else if (ddlUnitType.Text == "Power")
                {
                    power(originalValue);
                }
                else if (ddlUnitType.Text == "Temperature")
                {
                    temperature(originalValue);
                }
            }
            else
            {
                MessageBox.Show("Please enter a number.", "Format error");
                txtAmount1.Text = "0";
                lblAmountAns2.Text = "0";
            }
        }

        private void mnuItmExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void mnuItmAbout_Click(object sender, EventArgs e)
        {
            MessageBox.Show("The Converter application is an easy to use, simple application which allows " +
                "you to convert one unit of measurement to another.\r\n\r\n" +
                "The available conversions are Currency, Area, Energy, Length, Mass, Power & Temperature." +
                "\r\nTo convert units:\r\n\r\n1. Select the desired type of measurement." +
                "\r\n2. Select the first unit FROM which you would like to convert and the " +
                "second unit as the target measurement.\r\n3. Enter the amount and select the convert button." +
                "\r\n\r\nDeveloper: Mpumelelo Makhanya (NICO)", "NICO Converter 1.0");
        }
    }
}
