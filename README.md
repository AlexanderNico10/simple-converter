Simple Currency and Metric Data Converter
===

Licensing Information: READ LICENSE
---

Project source: https://AlexanderNico10@bitbucket.org/AlexanderNico10/simple-converter.git
---

File List
---

```
Converter.csproj
frmConverter.cs
frmConverter.Designer.cs
frmConverter.resx
Program.cs
LICENSE
README.md
.\bin
.\obj
.\Properties
```

This is an early concept for my Android app that uses the Yahoo! Finance online
converter to fetch live currency rates and display the values in local syntax
by using the Globalization class library.

How to run application
---

Import the application into Visual Studio, then start debugging. The TYPE dropdown
list represents the type of conversion. The first UNIT dropdown list is the initial
value and the second dropdwon list represents the final converted value. Simply
toggle all the lists and press convert to obtain the required value/amount.