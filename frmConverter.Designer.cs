﻿namespace Converter
{
    partial class frmConverter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConverter));
            this.btnConvert = new System.Windows.Forms.Button();
            this.lblType = new System.Windows.Forms.Label();
            this.lblUnit1 = new System.Windows.Forms.Label();
            this.lblAmount1 = new System.Windows.Forms.Label();
            this.lblUnit2 = new System.Windows.Forms.Label();
            this.txtAmount1 = new System.Windows.Forms.TextBox();
            this.ddlUnitType = new System.Windows.Forms.ComboBox();
            this.ddlUnit1 = new System.Windows.Forms.ComboBox();
            this.ddlUnit2 = new System.Windows.Forms.ComboBox();
            this.lblAmount2 = new System.Windows.Forms.Label();
            this.lblAmountAns2 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItmAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItmExit = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnConvert
            // 
            this.btnConvert.Location = new System.Drawing.Point(15, 181);
            this.btnConvert.Name = "btnConvert";
            this.btnConvert.Size = new System.Drawing.Size(184, 23);
            this.btnConvert.TabIndex = 0;
            this.btnConvert.Text = "Convert";
            this.btnConvert.UseVisualStyleBackColor = true;
            this.btnConvert.Click += new System.EventHandler(this.btnConvert_Click);
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(12, 33);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(34, 13);
            this.lblType.TabIndex = 1;
            this.lblType.Text = "Type:";
            // 
            // lblUnit1
            // 
            this.lblUnit1.AutoSize = true;
            this.lblUnit1.Location = new System.Drawing.Point(12, 62);
            this.lblUnit1.Name = "lblUnit1";
            this.lblUnit1.Size = new System.Drawing.Size(29, 13);
            this.lblUnit1.TabIndex = 2;
            this.lblUnit1.Text = "Unit:";
            // 
            // lblAmount1
            // 
            this.lblAmount1.AutoSize = true;
            this.lblAmount1.Location = new System.Drawing.Point(12, 89);
            this.lblAmount1.Name = "lblAmount1";
            this.lblAmount1.Size = new System.Drawing.Size(46, 13);
            this.lblAmount1.TabIndex = 3;
            this.lblAmount1.Text = "Amount:";
            // 
            // lblUnit2
            // 
            this.lblUnit2.AutoSize = true;
            this.lblUnit2.Location = new System.Drawing.Point(12, 115);
            this.lblUnit2.Name = "lblUnit2";
            this.lblUnit2.Size = new System.Drawing.Size(29, 13);
            this.lblUnit2.TabIndex = 4;
            this.lblUnit2.Text = "Unit:";
            // 
            // txtAmount1
            // 
            this.txtAmount1.Location = new System.Drawing.Point(78, 86);
            this.txtAmount1.Name = "txtAmount1";
            this.txtAmount1.Size = new System.Drawing.Size(121, 20);
            this.txtAmount1.TabIndex = 6;
            this.txtAmount1.Text = "0";
            // 
            // ddlUnitType
            // 
            this.ddlUnitType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlUnitType.FormattingEnabled = true;
            this.ddlUnitType.Location = new System.Drawing.Point(78, 30);
            this.ddlUnitType.Name = "ddlUnitType";
            this.ddlUnitType.Size = new System.Drawing.Size(121, 21);
            this.ddlUnitType.TabIndex = 8;
            // 
            // ddlUnit1
            // 
            this.ddlUnit1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlUnit1.FormattingEnabled = true;
            this.ddlUnit1.Location = new System.Drawing.Point(78, 59);
            this.ddlUnit1.Name = "ddlUnit1";
            this.ddlUnit1.Size = new System.Drawing.Size(121, 21);
            this.ddlUnit1.TabIndex = 9;
            // 
            // ddlUnit2
            // 
            this.ddlUnit2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlUnit2.FormattingEnabled = true;
            this.ddlUnit2.Location = new System.Drawing.Point(78, 112);
            this.ddlUnit2.Name = "ddlUnit2";
            this.ddlUnit2.Size = new System.Drawing.Size(121, 21);
            this.ddlUnit2.TabIndex = 10;
            // 
            // lblAmount2
            // 
            this.lblAmount2.AutoSize = true;
            this.lblAmount2.Location = new System.Drawing.Point(12, 142);
            this.lblAmount2.Name = "lblAmount2";
            this.lblAmount2.Size = new System.Drawing.Size(46, 13);
            this.lblAmount2.TabIndex = 11;
            this.lblAmount2.Text = "Amount:";
            // 
            // lblAmountAns2
            // 
            this.lblAmountAns2.AutoSize = true;
            this.lblAmountAns2.Location = new System.Drawing.Point(75, 142);
            this.lblAmountAns2.Name = "lblAmountAns2";
            this.lblAmountAns2.Size = new System.Drawing.Size(13, 13);
            this.lblAmountAns2.TabIndex = 12;
            this.lblAmountAns2.Text = "0";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(214, 24);
            this.menuStrip1.TabIndex = 13;
            this.menuStrip1.Text = "mnuMain";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItmAbout,
            this.mnuItmExit});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // mnuItmAbout
            // 
            this.mnuItmAbout.Name = "mnuItmAbout";
            this.mnuItmAbout.Size = new System.Drawing.Size(107, 22);
            this.mnuItmAbout.Text = "About";
            this.mnuItmAbout.Click += new System.EventHandler(this.mnuItmAbout_Click);
            // 
            // mnuItmExit
            // 
            this.mnuItmExit.Name = "mnuItmExit";
            this.mnuItmExit.Size = new System.Drawing.Size(107, 22);
            this.mnuItmExit.Text = "Exit";
            this.mnuItmExit.Click += new System.EventHandler(this.mnuItmExit_Click);
            // 
            // frmConverter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(214, 207);
            this.Controls.Add(this.lblAmountAns2);
            this.Controls.Add(this.lblAmount2);
            this.Controls.Add(this.ddlUnit2);
            this.Controls.Add(this.ddlUnit1);
            this.Controls.Add(this.ddlUnitType);
            this.Controls.Add(this.txtAmount1);
            this.Controls.Add(this.lblUnit2);
            this.Controls.Add(this.lblAmount1);
            this.Controls.Add(this.lblUnit1);
            this.Controls.Add(this.lblType);
            this.Controls.Add(this.btnConvert);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmConverter";
            this.Text = "Converter ";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnConvert;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.Label lblUnit1;
        private System.Windows.Forms.Label lblAmount1;
        private System.Windows.Forms.Label lblUnit2;
        private System.Windows.Forms.TextBox txtAmount1;
        private System.Windows.Forms.ComboBox ddlUnitType;
        private System.Windows.Forms.ComboBox ddlUnit1;
        private System.Windows.Forms.ComboBox ddlUnit2;
        private System.Windows.Forms.Label lblAmount2;
        private System.Windows.Forms.Label lblAmountAns2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnuItmAbout;
        private System.Windows.Forms.ToolStripMenuItem mnuItmExit;
    }
}

